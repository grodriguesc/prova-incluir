import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  regiaoSelecionada: any;
  paisSelecionado:any;
  resultado: any;
  dadosPais: any;
  habilitaBotao: boolean = false;

  constructor(
    private http:HttpClient,
    private router:Router
    ) {}

  customPopoverOptions: any = {
    header: 'Região',
  };

  async carregaRegiao(){

   await this.http.get(`https://restcountries.eu/rest/v2/region/${this.regiaoSelecionada}`).subscribe((data) => {
   
    this.resultado = data;

    });
    
  }

  async carregaPais(){

    this.habilitaBotao = true;

    await this.http.get(`https://restcountries.eu/rest/v2/name/${this.paisSelecionado}`).subscribe((data) => {

      this.dadosPais = data;
  
      });

  }

  async onClick(){

    if(this.habilitaBotao){
      this.router.navigateByUrl(`paises/${this.paisSelecionado}`)
    }
    
  }

  loadFlags() {
    setTimeout(()=>{ 
      let countries = this.resultado;

     let radios=document.getElementsByClassName('alert-radio-label');
     for (let index = 1; index < radios.length; index++) {
        let element = radios[index];
        
        element.innerHTML=element.innerHTML.concat('<img class="country-image" style="width: 30px;height:16px;" src="'+countries[index-1].flag+'" />');
      }
  }, 50);
  }
}
